﻿using Microsoft.EntityFrameworkCore;

namespace Domain
{
    public class Context : DbContext
    {
        public Context()
        {
            Database.EnsureCreated();
        }
        public DbSet<User> Users { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=DESKTOP-5O7RK02\NURYM;Database=Users;Trusted_connection=true;");
        }
    }
}