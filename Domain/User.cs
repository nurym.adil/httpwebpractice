﻿using System;

namespace Domain
{
    public class User
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return $"{Id.ToString()} {Name} {Password}";
        }
    }
}
