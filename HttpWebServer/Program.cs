﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace HttpWebServer
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Server server = new Server();
           await server.Listen();
        }

    }
}

