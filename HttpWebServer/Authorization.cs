﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpWebServer
{
    public class Authorization
    {
        public async Task<bool> LoginAsync(User user)
        {
            using var context = new Context();
            var users = await context.Users.ToListAsync();
            foreach (var element in users)
            {
                if (element.Name == user.Name && element.Password == element.Password)
                {
                    return true;
                }
            }
            return false;
        }
        public async Task<bool> Register(User user)
        {
            using var context = new Context();
            var users = context.Users.ToList();
            foreach (var element in users)
            {
                if (element.Name == user.Name)
                {
                    return false;
                }
            }

            await context.Users.AddAsync(user);
            await context.SaveChangesAsync();
            return true;
        }
    }
}
