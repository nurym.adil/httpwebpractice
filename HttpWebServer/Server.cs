﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace HttpWebServer
{
    public class Server
    {
        public static HttpListener listener;
        public static string url = "http://localhost/";

        public Server()
        {
            listener = new HttpListener();
            listener.Prefixes.Add(url);
        }

        public async Task Listen()
        {
            listener.Start();
            Authorization authorization = new Authorization();
            while (true)
            {
                try
                {
                    var context = await listener.GetContextAsync();
                    var request = context.Request;
                    var response = context.Response;
                    Console.WriteLine(request.RawUrl);

                    using var dbContext = new Context();
                    if (request.RawUrl == "/users/signup")
                    {
                        using var reader = new StreamReader(request.InputStream, request.ContentEncoding);
                        string text = reader.ReadToEnd();

                        var user = JsonConvert.DeserializeObject<User>(text);

                        if (await authorization.Register(user))
                        {
                            await SendStringResponse(context.Response, "Операция выполнено!");
                        }
                        else
                        {
                            await SendStringResponse(context.Response, "Операция не выполнено!\nПользователь такими данными уже существует");

                        }
                    }
                    else if (request.RawUrl == "/users/auth")
                    {
                        using var reader = new StreamReader(request.InputStream, request.ContentEncoding);
                        string text = reader.ReadToEnd();

                        var user = JsonConvert.DeserializeObject<User>(text);

                        if (await authorization.LoginAsync(user))
                        {
                            await SendStringResponse(context.Response, "Операция выполнено!");

                        }
                        else
                        {
                            await SendStringResponse(context.Response, "Операция не выполнено!\nПроверьте данные");
                        }

                    }
                    else
                    {
                        await SendStringResponse(context.Response, " Некоректный запрос!");
                    }

                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }

        public async Task SendStringResponse(HttpListenerResponse response, string text)
        {
            using var stream = response.OutputStream;
            var buffer = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(text));
            await stream.WriteAsync(buffer, 0, buffer.Length);

        }
    }
}