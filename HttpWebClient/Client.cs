﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Domain;
using Newtonsoft.Json;

namespace HttpWebClient
{
    public class Client : IDisposable
    {
        public HttpClient HttpClient { get; set; }


        private int Menu(params string[] choices)
        {
            foreach (var choice in choices)
            {
                Console.WriteLine(choice);
            }

            if (int.TryParse(Console.ReadLine(), out int result))
            {
                Console.Clear();
                return result;
            }

            return -1;
        }

        public Client()
        {
            HttpClient = new HttpClient();
        }

        public async Task Start(string uri)
        {
            try
            {
                while (true)
                {
                    int menu = Menu("1.Регистрация", "2.Вход", "3.Выход");
                    if (menu == 1)
                    {
                        var user = CreateUser();
                        await SendRequestAsync($"{uri}users/signup",user);
                    }
                    else if (menu == 2)
                    {
                        var user = CreateUser();
                        await SendRequestAsync($"{uri}users/auth",user);
                    }
                    else if (menu == 3)
                    {
                        Environment.Exit(0);
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
            }
        }
        public async Task SendRequestAsync(string requestUri, User user)
        {

            Console.Clear();
            var json = JsonConvert.SerializeObject(user);
            var response = await HttpClient.PostAsync(requestUri, new StringContent(json, Encoding.UTF8, "application/json"));
            var text = JsonConvert.DeserializeObject<string>(await response.Content.ReadAsStringAsync());
            Console.WriteLine(text);
        }


        public User CreateUser()
        {
            Console.Clear();

            Console.WriteLine($"Введите логин:");
            string name = Console.ReadLine();
            Console.WriteLine($"Введите пароль");
            string password = Console.ReadLine();
            return new User { Name = name, Password = password };
        }

        public void Dispose()
        {
            HttpClient?.Dispose();
        }
    }
}