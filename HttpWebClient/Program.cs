﻿using Domain;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HttpWebClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.ReadLine();
            using Client client = new Client();
           await client.Start("http://localhost/");

            Console.ReadLine();

        }

    }
}
